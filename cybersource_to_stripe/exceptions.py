class MigrationError(Exception):
    message: str


class CybersourceSubscriptionCancelFailed(MigrationError):
    def __init__(self, subscription_id, decision):
        self.message = f'Failed to cancel subscription: subscription={subscription_id} decision={decision}'
        super().__init__(self.message)


class CybersourceSubscriptionRetrieveFailed(MigrationError):
    def __init__(self, subscription_id, reason_code):
        self.message = f'Failed to retrieve subscription: subscription={subscription_id} reason_code={reason_code}'
        super().__init__(self.message)


class InvalidAmount(MigrationError):
    def __init__(self, subscription_id, amount):
        self.message = f'Failed to retrieve price: subscription={subscription_id} amount={amount}'
        super().__init__(self.message)


class DuplicateStripeCustomers(MigrationError):
    def __init__(self, email):
        self.message = f'Multiple Stripe customers found for email: email={email}'
        super().__init__(self.message)
