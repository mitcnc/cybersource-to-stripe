import csv
import datetime
import logging
import os
import sys
from typing import Iterator

from google.cloud import bigquery
from peewee import DoesNotExist, SqliteDatabase, fn

from cybersource_to_stripe.models import Subscription, database_proxy


def setup_logging(log_filename):
    log_directory = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'logs')
    log_filepath = os.path.join(log_directory, log_filename)
    fh = logging.FileHandler(log_filepath)
    ch = logging.StreamHandler(sys.stdout)
    logging.basicConfig(
        format='%(asctime)s - %(filename)s - %(levelname)s - %(message)s',
        level=logging.INFO,
        handlers=(ch, fh)
    )


def setup_database(db_file_path: str):
    database = SqliteDatabase(db_file_path)
    database_proxy.initialize(database)
    database.create_tables([Subscription])


def daterange(start_date: datetime.date, end_date: datetime.date, reverse=False) -> Iterator[datetime.date]:
    """ Returns an iterator that iterates over all dates between start_date and *the day after* end_date.

    Notes:
        Sourced from https://stackoverflow.com/a/1060330/592820.
    """
    days = range(int((end_date - start_date).days) + 1)

    for n in reversed(days) if reverse else days:
        yield start_date + datetime.timedelta(days=n)


def get_stripe_card_brand(cybersource_card_type: str) -> str:
    return {
        '001': 'visa',
        '002': 'mastercard',
        '003': 'amex',
        '004': 'discover',
    }[cybersource_card_type]


def mark_duplicates():
    query = Subscription.select(Subscription.advance_id, fn.Count('*').alias('count')) \
        .where(Subscription.cybersource_subscription_id.is_null(False) &
               (Subscription.cybersource_subscription_id != '')) \
        .group_by(Subscription.advance_id) \
        .having(fn.Count('*') > 1) \
        .order_by(Subscription.advance_id)

    for row in query:
        advance_id = row.advance_id
        logging.info('Found user with duplicate subscriptions. advance_id=%s count=%d', advance_id, row.count)
        subscriptions = Subscription.select() \
            .where(
            (Subscription.advance_id == advance_id) &
            Subscription.cybersource_subscription_id.is_null(False)) \
            .order_by(Subscription.transaction_date.desc())

        start_year = None
        adjusted_billing_date = None
        primary_subscription = None

        for subscription in subscriptions:
            txn_date = subscription.transaction_date
            if adjusted_billing_date is None:
                # The first (most-recent) subscription is the primary since the
                # most-recent transaction should have a valid card.
                primary_subscription = subscription
                adjusted_billing_date = txn_date
                start_year = txn_date.year
            else:
                # Some users may have renewed before their renewal date, resulting in transactions with
                # different billing dates. We want to maximize the membership period by picking the latest
                # date in the latest year.
                if txn_date.replace(year=adjusted_billing_date.year) > adjusted_billing_date:
                    adjusted_billing_date = adjusted_billing_date.replace(
                        month=txn_date.month,
                        day=txn_date.day,
                    )

                start_year = min(adjusted_billing_date.year, txn_date.year)
                adjusted_billing_date = adjusted_billing_date.replace(
                    year=max(adjusted_billing_date.year, txn_date.year))

            if subscription.id != primary_subscription.id:
                subscription.duplicates_subscription = primary_subscription
                subscription.save()

        adjusted_end_date = adjusted_billing_date.replace(year=start_year + len(subscriptions))

        primary_subscription.adjusted_billing_date = adjusted_billing_date
        primary_subscription.adjusted_end_date = adjusted_end_date
        primary_subscription.save()


def load_amount_overrides(overrides_file_path: str):
    with open(overrides_file_path, 'r') as csv_file:
        reader = csv.DictReader(csv_file, fieldnames=('advance_id', 'amount',))
        for row in reader:
            subscription = Subscription.get(
                (Subscription.advance_id == row['advance_id']) &
                Subscription.duplicates_subscription.is_null()
            )
            subscription.amount_override = row['amount']
            subscription.save()


def load_additional_data_from_data_warehouse():
    client = bigquery.Client()
    query = 'SELECT advance_id, purchase_price, expires_at FROM `reporting.memberships_annotated`'
    query_job = client.query(query)

    for row in query_job.result():
        advance_id = row.advance_id

        try:
            subscription = Subscription.get(Subscription.advance_id == advance_id)

            if row.purchase_price >= 50:
                subscription.amount_override = row.purchase_price
            else:
                subscription.amount_override = None

            subscription.adjusted_end_date = row.expires_at.date() if row.expires_at else None
            subscription.save()
        except DoesNotExist:
            logging.debug('Subscription not found. advance_id=%s', advance_id)
