import datetime
import logging
import xml.etree.ElementTree as ET
from calendar import monthrange
from dataclasses import dataclass
from typing import List

from CyberSource import ReportDownloadsApi
from zeep import Client
from zeep.wsse import UsernameToken

from cybersource_to_stripe.exceptions import CybersourceSubscriptionCancelFailed, CybersourceSubscriptionRetrieveFailed


@dataclass
class RequestMetadata:
    transaction_date: datetime.date
    merchant_reference_number: str
    request_id: str
    price: str
    subscription_id: str


class Cybersource:
    def __init__(
        self,
        merchant_id: str,
        merchant_key_id: str,
        merchant_secret_key: str,
        soap_transaction_key: str,
        environment='CyberSource.Environment.PRODUCTION',
        soap_api_url='https://ics2wsa.ic3.com/commerce/1.x/transactionProcessor/CyberSourceTransaction_1.169.wsdl',
    ):
        self.soap_transaction_key = soap_transaction_key
        self.soap_api_url = soap_api_url
        self.merchant_id = merchant_id
        self.merchant_key_id = merchant_key_id
        self.merchant_secret_key = merchant_secret_key
        self.environment = environment
        self.client_config = {
            'authentication_type': 'http_signature',
            'enable_log': True,
            'log_file_name': 'cybersource',
            'log_maximum_size': 10487560,
            'merchantid': self.merchant_id,
            'merchant_keyid': self.merchant_key_id,
            'merchant_secretkey': self.merchant_secret_key,
            'run_environment': self.environment,
        }
        self.report_download_client = ReportDownloadsApi(self.client_config)
        self.soap_client = Client(self.soap_api_url, wsse=UsernameToken(self.merchant_id, self.soap_transaction_key))

    def get_request_metadata(self, day: datetime.date, report_name: str) -> List[RequestMetadata]:
        """ Retrieves request metadata (e.g., merchant reference number,
        subscription ID, amount) for all transactions created on the specified date.

        Args:
            day (datetime.date): Day requests were created.
            report_name (str): Name of the daily report that contains the transactions.

        Returns:
            RequestMetadata[]
        """

        # Download the report
        report_download_client = self.report_download_client

        logging.info('Downloading report %s for %s', report_name, day)
        __, __, body = report_download_client.download_report(
            str(day),
            report_name,
            organization_id=self.merchant_id
        )

        # Parse the report XML
        xml = ET.fromstring(body)
        path = 'Requests/Request[@Comments="Club of Northern California Membership Registration Form"]'
        request_nodes = xml.findall(path)
        request_metadata = []
        for node in request_nodes:
            merchant_reference_number = node.attrib['MerchantReferenceNumber']
            request_id = node.attrib['RequestID']

            bill_reply_flag = node.find('ApplicationReplies/ApplicationReply[@Name="ics_bill"]/RFlag')
            if (bill_reply_flag is not None) and ((bill_reply_flag.text or '').upper() == 'SOK'):
                price = ''
                subscription_id = node.attrib['SubscriptionID']

                # Get the amount
                amount_node = node.find('LineItems/LineItem/UnitPrice')
                if amount_node is None:
                    logging.warning(
                        'No amount found for transaction date=%s request_id=%s',
                        day,
                        request_id
                    )
                else:
                    price = amount_node.text or ''

                request_metadata.append(
                    RequestMetadata(
                        transaction_date=day,
                        merchant_reference_number=merchant_reference_number,
                        request_id=request_id,
                        price=price,
                        subscription_id=subscription_id
                    )
                )

        logging.info(
            'Recorded %d of %d transactions on %s',
            len(request_metadata),
            len(request_nodes),
            day
        )
        return request_metadata

    def get_subscription(self, merchant_reference_code, subscription_id):

        # For info on paySubscriptionRetrieveService, see
        # https://developer.cybersource.com/library/documentation/dev_guides/Recurring_Billing/SO_API/Recurring_Billing_SO_API.pdf
        response = self.soap_client.service.runTransaction(
            merchantID=self.merchant_id,
            merchantReferenceCode=merchant_reference_code,
            paySubscriptionRetrieveService={'run': 'true', },
            recurringSubscriptionInfo={
                'subscriptionID': subscription_id,
            }
        )

        subscription = response.paySubscriptionRetrieveReply

        reason_code = subscription.reasonCode
        if reason_code != 100:
            raise CybersourceSubscriptionRetrieveFailed(subscription_id, reason_code)

        subscription.customer_name = self.get_subscription_customer_name(subscription)
        subscription.card_expiration_date = self.get_subscription_card_expiration_date(subscription)

        return subscription

    def cancel_subscription(self, merchant_reference_code, subscription_id):
        response = self.soap_client.service.runTransaction(
            merchantID=self.merchant_id,
            merchantReferenceCode=merchant_reference_code,
            paySubscriptionUpdateService={'run': 'true', },
            recurringSubscriptionInfo={
                'status': 'cancel',
                'subscriptionID': subscription_id,
            },
        )

        decision = response.decision.upper()
        if decision == 'ACCEPT':
            logging.info(
                'Canceled Cybersource subscription merchant_ref_num=%s subscription_id=%s',
                merchant_reference_code,
                subscription_id
            )
        else:
            raise CybersourceSubscriptionCancelFailed(subscription_id, decision)

    @staticmethod
    def get_subscription_card_expiration_date(subscription):
        expiration_year = int(subscription.cardExpirationYear)
        expiration_month = int(subscription.cardExpirationMonth)
        expiration_day = monthrange(expiration_year, expiration_month)[1]
        return datetime.date(
            year=expiration_year,
            month=expiration_month,
            day=expiration_day
        )

    @staticmethod
    def get_subscription_customer_name(subscription):
        return f'{subscription.firstName} {subscription.lastName}'

    @staticmethod
    def is_card_expired(subscription) -> bool:
        return subscription.card_expiration_date < datetime.date.today()
