from peewee import (
    BooleanField, CharField, DatabaseProxy, DateTimeField, DecimalField, ForeignKeyField, Model, TextField
)

database_proxy = DatabaseProxy()


class BaseModel(Model):
    class Meta:
        database = database_proxy


class Subscription(BaseModel):
    transaction_date = DateTimeField()
    merchant_reference_number = CharField()
    request_id = CharField()
    amount = DecimalField(decimal_places=2)
    amount_override = DecimalField(decimal_places=2, null=True)
    cybersource_subscription_id = CharField(unique=True, null=True)
    cybersource_subscription_cancelled = BooleanField(default=False)
    cybersource_status = CharField(null=True)
    advance_id = CharField(index=True)
    email = CharField(null=True)
    stripe_customer_id = CharField(null=True)
    stripe_payment_method_id = CharField(null=True)
    stripe_subscription_id = CharField(null=True)
    error = TextField(null=True)
    duplicates_subscription = ForeignKeyField('self', null=True, backref='duplicates')
    adjusted_billing_date = DateTimeField(null=True)
    adjusted_end_date = DateTimeField(null=True)
