import datetime
import logging
from dataclasses import dataclass
from typing import Dict, Optional

import stripe
from stripe.error import CardError

from cybersource_to_stripe.exceptions import DuplicateStripeCustomers

logger = logging.getLogger(__name__)


@dataclass
class Address:
    city: str
    country: str
    line1: str
    line2: Optional[str]
    postal_code: str
    state: str


class Stripe:
    def __init__(self, api_key: str, livemode=False):
        stripe.api_key = api_key
        self.livemode = livemode

    def get_or_create_stripe_customer(
        self,
        email: str,
        name: str,
        phone_number: Optional[str] = None,
        address: Optional[Address] = None
    ) -> dict:
        email = email.lower()

        customers = stripe.Customer.list(email=email).data
        num_customers = len(customers)
        if num_customers == 1:
            customer = customers[0]
            logger.info('Found Stripe customer for email. customer_id=%s email=%s', customer['id'], email)
            return customer

        if num_customers > 1:
            raise DuplicateStripeCustomers(email)

        customer = stripe.Customer.create(
            description=name,
            email=email,
            name=name,
            phone=phone_number,
            address=address.__dict__ if address else None
        )
        logger.info('Created Stripe customer for email. customer_id=%s email=%s', customer['id'], email)

        return customer

    def get_or_create_card_payment_method(
        self,
        customer: dict,
        number: str,
        brand: str,
        expiration_month: int,
        expiration_year: int
    ) -> Optional[dict]:
        payment_method = None
        customer_id = customer['id']

        # Check for an existing PaymentMethod
        payment_methods = stripe.PaymentMethod.list(
            customer=customer_id,
            type='card',
        )

        if not self.livemode:
            brand = 'visa'
            number = '4242424242424242'

        reference = {
            'brand': brand,
            'exp_month': expiration_month,
            'exp_year': expiration_year,
            'last4': number[-4:],
        }

        for candidate in payment_methods:
            if reference.items() <= candidate['card'].items():
                payment_method = candidate
                logger.info(
                    'Found existing PaymentMethod customer=%s payment_method=%s',
                    customer_id,
                    payment_method['id']
                )
                break

        if payment_method is None:
            try:
                payment_method = stripe.PaymentMethod.create(
                    type='card',
                    card={
                        'number': number,
                        'exp_month': expiration_month,
                        'exp_year': expiration_year,
                    },
                )
                stripe.PaymentMethod.attach(
                    payment_method['id'],
                    customer=customer_id,
                )
                logger.info(
                    'Created new PaymentMethod customer=%s payment_method=%s',
                    customer_id,
                    payment_method['id']
                )
            except CardError:
                logger.exception(
                    'Failed to attach PaymentMethod customer=%s payment_method=%s',
                    customer_id,
                    payment_method['id'] if payment_method else None
                )
                return None

        if not customer['invoice_settings']['default_payment_method']:
            stripe.Customer.modify(
                customer_id,
                invoice_settings={
                    'default_payment_method': payment_method['id'],
                },
            )
            logger.info(
                'Set default PaymentMethod customer=%s payment_method=%s',
                customer_id,
                payment_method['id']
            )
        return payment_method

    def get_or_create_subscription(
        self,
        customer_id: str,
        stripe_price: str,
        metadata_unique_id: Dict[str, str],
        metadata: Optional[Dict[str, str]] = None,
        billing_cycle_anchor: Optional[datetime.datetime] = None,
        trial_end: Optional[datetime.datetime] = None,
        backdate_start_date: Optional[int] = None,
        cancel_at_period_end: bool = False
    ) -> dict:
        metadata = (metadata or {})
        metadata.update(metadata_unique_id)

        subscription = None

        # Check for an existing subscription
        subscriptions = stripe.Subscription.list(customer=customer_id)
        metadata_unique_id_items = metadata_unique_id.items()
        for candidate in subscriptions:
            if all(item in candidate['metadata'].items() for item in metadata_unique_id_items):
                subscription = candidate
                logger.info(
                    'Found existing Stripe subscription customer=%s stripe_subscription=%s unique_metadata=%s',
                    customer_id,
                    subscription['id'],
                    metadata_unique_id
                )
                break

        if subscription is None:
            subscription = stripe.Subscription.create(
                customer=customer_id,
                backdate_start_date=backdate_start_date,
                billing_cycle_anchor=int(billing_cycle_anchor.timestamp()) if billing_cycle_anchor else None,
                trial_end=int(trial_end.timestamp()) if trial_end else None,
                items=[
                    {
                        'price': stripe_price,
                        'quantity': 1,
                    }
                ],
                metadata=metadata,
                cancel_at_period_end=cancel_at_period_end,
            )

            logger.info(
                'Created new Stripe subscription customer=%s stripe_subscription=%s unique_metadata=%s',
                customer_id,
                subscription['id'],
                metadata_unique_id
            )

        return subscription
