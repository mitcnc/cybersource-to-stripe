import logging

import environ
from google.cloud import bigquery

from cybersource_to_stripe.stripe import Stripe
from cybersource_to_stripe.utils import setup_logging

env = environ.Env(
    LOG_FILENAME=(str, 'mitcnc_migrate_lifetime_memberships.log'),
    STRIPE_LIVE_MODE=(bool, False),
)
environ.Env.read_env()

setup_logging(env('LOG_FILENAME'))
logger = logging.getLogger(__name__)


def main():
    stripe_price = env.str('STRIPE_LIFETIME_MEMBERSHIP_PRICE')
    client = bigquery.Client()
    query = """
    SELECT
      m.advance_id,
      m.first_name,
      m.last_name,
      LOWER(m.email) AS email,
      last_purchased_at,
      c.id AS customer_id,
      s.id AS subscription_id
    FROM
      reporting.memberships m
    LEFT JOIN
      reporting.stripe_customers c
    ON
      LOWER(m.email) = c.email
    LEFT JOIN
      reporting.stripe_subscriptions s
    ON
      s.customer = c.id
    WHERE
      is_lifetime IS TRUE
      AND m.email IS NOT NULL
      AND m.email <> ''
      AND s.id IS NULL
    """
    query_job = client.query(query)

    stripe_client = Stripe(env.str('STRIPE_API_KEY'), livemode=env('STRIPE_LIVE_MODE'))

    for row in query_job.result():
        customer = stripe_client.get_or_create_stripe_customer(
            email=row.email,
            name=f'{row.first_name} {row.last_name}'
        )
        stripe_client.get_or_create_subscription(
            customer_id=customer['id'],
            stripe_price=stripe_price,
            backdate_start_date=int(row.last_purchased_at.timestamp()),
            metadata_unique_id={'note': f'Lifetime migration for {row.email}'}
        )


if __name__ == '__main__':
    main()
