import logging

import environ

from cybersource_to_stripe.utils import (
    load_additional_data_from_data_warehouse, mark_duplicates, setup_database, setup_logging
)

env = environ.Env(
    DB_FILE_PATH=(str, 'output/subscriptions.db'),
    LOG_FILENAME=(str, 'clean_data.log'),
    OVERRIDES_FILE_PATH=(str, 'output/overrides.csv'),
)
environ.Env.read_env()

setup_logging(env('LOG_FILENAME'))
logger = logging.getLogger(__name__)

DB_FILE_PATH = env('DB_FILE_PATH')
OVERRIDES_FILE_PATH = env('OVERRIDES_FILE_PATH')


def main():
    setup_database(DB_FILE_PATH)

    mark_duplicates()
    load_additional_data_from_data_warehouse()


if __name__ == '__main__':
    main()
