import logging

import environ

from cybersource_to_stripe.cybersource import Cybersource
from cybersource_to_stripe.models import Subscription
from cybersource_to_stripe.utils import setup_database, setup_logging

env = environ.Env(
    DB_FILE_PATH=(str, 'output/subscriptions.db'),
    LOG_FILENAME=(str, 'cancel_cybersource_subscriptions.log'),
)
environ.Env.read_env()

setup_logging(env('LOG_FILENAME'))
logger = logging.getLogger(__name__)

DB_FILE_PATH = env('DB_FILE_PATH')


def main():
    cybersource = Cybersource(
        merchant_id=env.str('CS_MERCHANT_ID'),
        merchant_key_id=env.str('CS_MERCHANT_KEYID'),
        merchant_secret_key=env.str('CS_MERCHANT_SECRETKEY'),
        environment=env.str('CS_ENVIRONMENT'),
        soap_transaction_key=env.str('CS_TRANSACTION_KEY')
    )

    setup_database(DB_FILE_PATH)

    query = Subscription.select() \
        .where(
        (Subscription.cybersource_subscription_cancelled == 0) &
        Subscription.cybersource_subscription_id.is_null(False)) \
        .order_by(Subscription.transaction_date)

    for db_subscription in query:
        merchant_reference_number = db_subscription.merchant_reference_number
        subscription_id = db_subscription.cybersource_subscription_id

        cybersource.cancel_subscription(merchant_reference_number, subscription_id)
        db_subscription.cybersource_status = 'CANCELED'
        db_subscription.cybersource_subscription_cancelled = True
        db_subscription.save()


if __name__ == '__main__':
    main()
