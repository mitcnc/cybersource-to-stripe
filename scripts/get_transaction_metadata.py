import csv
import datetime
import logging
import os
import re
from decimal import Decimal
from typing import Dict

import environ
from dateutil.relativedelta import relativedelta
from google.cloud import bigquery

from cybersource_to_stripe.cybersource import Cybersource, RequestMetadata
from cybersource_to_stripe.models import Subscription
from cybersource_to_stripe.utils import daterange, mark_duplicates, setup_database, setup_logging

env = environ.Env(
    LOG_FILENAME=(str, 'get_transaction_metadata.log'),
    REPORT_NAME=(str, 'TDR_MIT'),
)
environ.Env.read_env()

setup_logging(env('LOG_FILENAME'))
logger = logging.getLogger(__name__)

# This is the daily report from which we will pull transaction data
REPORT_NAME = env('REPORT_NAME')

# We want the data for the past year since our memberships rollover annually.
LOOKBACK_DELTA = relativedelta(years=1)

CSV_FIELD_NAMES = ['transaction_date', 'merchant_reference_number', 'request_id', 'price',
                   'subscription_id', 'advance_id', 'email', ]

# DO NOT TOUCH: We will cache a mapping of Advance IDs to emails here.
ADVANCE_ID_TO_EMAIL: Dict[str, str] = {}


def get_additional_data(request_metadata: RequestMetadata) -> dict:
    merchant_reference_number = request_metadata.merchant_reference_number
    advance_id = ''
    try:
        matches = re.match(r'.*_(\d+)', merchant_reference_number)
        assert matches

        advance_id = matches[1]
    except TypeError:
        logging.warning(
            'Failed to parse Advance ID merchant_reference_number=%s request_id=%s',
            merchant_reference_number,
            request_metadata.request_id
        )

    return {
        'advance_id': advance_id,
        'email': ADVANCE_ID_TO_EMAIL.get(advance_id, ''),
    }


def cache_email_lookup_table():
    client = bigquery.Client()
    query = 'SELECT advance_id, email FROM `reporting.memberships_annotated`'
    query_job = client.query(query)
    for row in query_job.result():
        ADVANCE_ID_TO_EMAIL[row.advance_id] = row.email


def main():
    timestamp = int(datetime.datetime.now().timestamp())
    output_file = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'output', f'{timestamp}-transactions.csv')
    db_file_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'output', 'subscriptions.db')

    setup_database(db_file_path)

    # Initialize the Cybersource client
    cybersource = Cybersource(
        merchant_id=env.str('CS_MERCHANT_ID'),
        merchant_key_id=env.str('CS_MERCHANT_KEYID'),
        merchant_secret_key=env.str('CS_MERCHANT_SECRETKEY'),
        environment=env.str('CS_ENVIRONMENT'),
        soap_transaction_key=env.str('CS_TRANSACTION_KEY')
    )

    # Cache the email lookup table so we minimize our query count
    cache_email_lookup_table()

    # Get the data!
    today = datetime.date.today()
    start_date = today - LOOKBACK_DELTA

    for subscribe_date in daterange(start_date, today, reverse=True):
        with open(output_file, 'a+') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=CSV_FIELD_NAMES)

            try:
                metadata = cybersource.get_request_metadata(subscribe_date, REPORT_NAME)

                for datum in metadata:
                    additional_data = get_additional_data(datum)
                    row = vars(datum)
                    row.update(additional_data)

                    defaults = {
                        'transaction_date': datum.transaction_date,
                        'merchant_reference_number': datum.merchant_reference_number,
                        'request_id': datum.request_id,
                        'amount': Decimal(datum.price),
                    }
                    defaults.update(additional_data)

                    Subscription.get_or_create(
                        cybersource_subscription_id=datum.subscription_id or None,
                        defaults=defaults
                    )

                    writer.writerow(row)
            except BaseException:
                logging.exception('Failed to process data for %s', subscribe_date)

    mark_duplicates()


if __name__ == '__main__':
    main()
