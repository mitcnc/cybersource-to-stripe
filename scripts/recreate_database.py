import csv
import os
from decimal import Decimal

from cybersource_to_stripe.models import Subscription
from cybersource_to_stripe.utils import mark_duplicates, setup_database
from scripts.get_transaction_metadata import CSV_FIELD_NAMES


def load_csv_in_database():
    csv_file_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'output', '1604083528-transactions.csv')

    with open(csv_file_path, 'r') as csv_file:
        reader = csv.DictReader(csv_file, fieldnames=CSV_FIELD_NAMES)
        for row in reader:
            merchant_reference_number = row['merchant_reference_number']
            subscription_id = row['subscription_id'] or None
            email = row['email']

            Subscription.get_or_create(
                cybersource_subscription_id=subscription_id,
                defaults={
                    'transaction_date': row['transaction_date'],
                    'merchant_reference_number': merchant_reference_number,
                    'request_id': row['request_id'],
                    # Round to the nearest whole number
                    'amount': Decimal(int(Decimal(row['price']))),
                    'advance_id': row['advance_id'],
                    'email': email,
                }
            )


def main():
    db_file_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'output', 'subscriptions.db')
    setup_database(db_file_path)
    load_csv_in_database()
    mark_duplicates()


if __name__ == '__main__':
    main()
