import datetime
import logging
from typing import Optional

import environ
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta

from cybersource_to_stripe.cybersource import Cybersource
from cybersource_to_stripe.exceptions import InvalidAmount, MigrationError
from cybersource_to_stripe.models import Subscription
from cybersource_to_stripe.stripe import Address, Stripe
from cybersource_to_stripe.utils import get_stripe_card_brand, setup_database, setup_logging

env = environ.Env(
    CANCEL_CYBERSOURCE_SUBSCRIPTIONS=(bool, False),
    CREATE_STRIPE_SUBSCRIPTIONS=(bool, False),
    CREATE_SUBSCRIPTIONS_FOR_EXPIRED_CARDS=(bool, False),
    DB_FILE_PATH=(str, 'output/subscriptions.db'),
    LOG_FILENAME=(str, 'transfer_data.log'),
    OVERRIDES_FILE_PATH=(str, 'output/overrides.csv'),
    STRIPE_LIVE_MODE=(bool, False),
)
environ.Env.read_env()

setup_logging(env('LOG_FILENAME'))
logger = logging.getLogger(__name__)

OVERRIDES_FILE_PATH = env('OVERRIDES_FILE_PATH')
DB_FILE_PATH = env('DB_FILE_PATH')

CREATE_STRIPE_SUBSCRIPTIONS = env('CREATE_STRIPE_SUBSCRIPTIONS')
STRIPE_LIVE_MODE = env('STRIPE_LIVE_MODE')
CANCEL_CYBERSOURCE_SUBSCRIPTIONS = env('CANCEL_CYBERSOURCE_SUBSCRIPTIONS')
CREATE_SUBSCRIPTIONS_FOR_EXPIRED_CARDS = env('CREATE_SUBSCRIPTIONS_FOR_EXPIRED_CARDS')

PRICE_MAP = {
    '30.00': 'price_1HQiKkCAwi9Ti4sNmXrfw8Lq',
    '50.00': 'price_1HQOrgCAwi9Ti4sN3yrAIBCB',
    '100.00': 'price_1HQOrdCAwi9Ti4sNi3suOw1n',
    '200.00': 'price_1HQOraCAwi9Ti4sNBJONrQQu',
    '500.00': 'price_1HQOrWCAwi9Ti4sNL1qY4QZ2',
    '1200.00': 'price_1HQOrjCAwi9Ti4sNmZulkTvZ',
    '3600.00': 'price_1HQOrCCAwi9Ti4sNsS5wVehZ',
} if STRIPE_LIVE_MODE else {
    '30.00': 'price_1HQiKHCAwi9Ti4sNhPBn03kC',
    '50.00': 'price_1GvfsYCAwi9Ti4sNjHt6djSk',
    '100.00': 'price_1GvfssCAwi9Ti4sNbrmnSwPF',
    '200.00': 'price_1Gvft7CAwi9Ti4sNbZnKHHCC',
    '500.00': 'price_1GvftqCAwi9Ti4sN4kAHHYKD',
    '1200.00': 'price_1GyB2GCAwi9Ti4sNokX32imb',
    '3600.00': 'price_1GyB2lCAwi9Ti4sNkNRY048w',
}


def transfer_to_stripe(subscription: Subscription, cybersource_subscription) -> Optional[dict]:
    stripe_client = Stripe(env.str('STRIPE_API_KEY'), livemode=STRIPE_LIVE_MODE)

    # Ensure we use the @alum.mit.edu address
    cybersource_subscription.email = subscription.email
    start_date = subscription.adjusted_billing_date or parse(cybersource_subscription.startDate)
    end_date = subscription.adjusted_end_date
    amount = round(subscription.amount_override or subscription.amount, 2)

    stripe_customer = stripe_client.get_or_create_stripe_customer(
        email=cybersource_subscription.email.lower(),
        name=cybersource_subscription.customer_name,
        phone_number=cybersource_subscription.phoneNumber,
        address=Address(
            city=cybersource_subscription.city,
            country=cybersource_subscription.country,
            line1=cybersource_subscription.street1,
            line2=cybersource_subscription.street2,
            postal_code=cybersource_subscription.postalCode,
            state=cybersource_subscription.state,
        )
    )
    subscription.stripe_customer_id = stripe_customer['id']
    subscription.save()

    payment_method = None
    if not Cybersource.is_card_expired(cybersource_subscription):
        payment_method = stripe_client.get_or_create_card_payment_method(
            customer=stripe_customer,
            expiration_month=int(cybersource_subscription.cardExpirationMonth),
            expiration_year=int(cybersource_subscription.cardExpirationYear),
            number=cybersource_subscription.cardAccountNumber,
            brand=get_stripe_card_brand(cybersource_subscription.cardType)
        )

        if payment_method:
            subscription.stripe_payment_method_id = payment_method['id']
            subscription.save()

    stripe_price = PRICE_MAP.get(str(amount))

    if not stripe_price:
        raise InvalidAmount(cybersource_subscription.subscriptionID, amount)

    if CREATE_STRIPE_SUBSCRIPTIONS \
        and (CREATE_SUBSCRIPTIONS_FOR_EXPIRED_CARDS or payment_method is not None) \
            and (stripe_price is not None):
        billing_cycle_anchor = end_date

        if not billing_cycle_anchor:
            billing_cycle_anchor = start_date
            now = datetime.datetime.now()
            while billing_cycle_anchor < now:
                billing_cycle_anchor = billing_cycle_anchor + relativedelta(years=1)

        metadata = {'cybersource_id': cybersource_subscription.subscriptionID}
        stripe_subscription = stripe_client.get_or_create_subscription(
            stripe_customer['id'],
            stripe_price,
            billing_cycle_anchor=billing_cycle_anchor,
            metadata_unique_id=metadata,
            metadata=metadata,
            # Setup a trial that ends on the next rollover date. This ensures
            # we don't charge again for subscriptions that are already paid.
            trial_end=billing_cycle_anchor,
        )
        subscription.stripe_subscription_id = stripe_subscription['id']
        subscription.save()

        return stripe_subscription

    return None


def process_transfer(cybersource, db_subscription):
    merchant_reference_number = db_subscription.merchant_reference_number
    subscription_id = db_subscription.cybersource_subscription_id

    db_subscription.error = None

    try:
        logger.info(
            'Loading Cybersource subscription merchant_ref_num=%s subscription_id=%s',
            merchant_reference_number,
            subscription_id
        )

        cybersource_subscription = cybersource.get_subscription(merchant_reference_number, subscription_id)

        status = cybersource_subscription.status.upper()
        db_subscription.cybersource_status = status
        db_subscription.save()

        if status == 'CURRENT':
            if not cybersource.is_card_expired(cybersource_subscription) or CREATE_SUBSCRIPTIONS_FOR_EXPIRED_CARDS:
                stripe_subscription = transfer_to_stripe(db_subscription, cybersource_subscription)

                if stripe_subscription and CANCEL_CYBERSOURCE_SUBSCRIPTIONS \
                        and not db_subscription.cybersource_subscription_cancelled:
                    cybersource.cancel_subscription(merchant_reference_number, subscription_id)
                    db_subscription.cybersource_subscription_cancelled = True
                    db_subscription.save()
            else:
                db_subscription.error = 'Card expired.'
                logger.info(
                    'Card expired. Skipping. merchant_ref_num=%s subscription_id=%s',
                    merchant_reference_number,
                    subscription_id
                )
        else:
            db_subscription.cybersource_subscription_cancelled = True
            db_subscription.error = 'Subscription is not current.'
            logger.info(
                'Subscription is not current. Skipping. merchant_ref_num=%s subscription_id=%s',
                merchant_reference_number,
                subscription_id
            )

    except MigrationError as e:
        logging.exception(e)
        db_subscription.error = e.message
    finally:
        db_subscription.save()


def main():
    # Initialize the Cybersource client
    cybersource = Cybersource(
        merchant_id=env.str('CS_MERCHANT_ID'),
        merchant_key_id=env.str('CS_MERCHANT_KEYID'),
        merchant_secret_key=env.str('CS_MERCHANT_SECRETKEY'),
        environment=env.str('CS_ENVIRONMENT'),
        soap_transaction_key=env.str('CS_TRANSACTION_KEY')
    )

    setup_database(DB_FILE_PATH)

    query = Subscription.select() \
        .where(
        (Subscription.cybersource_subscription_cancelled == 0) &
        Subscription.cybersource_subscription_id.is_null(False) &
        Subscription.duplicates_subscription.is_null() &
        Subscription.stripe_subscription_id.is_null() &
        (Subscription.adjusted_end_date > datetime.datetime.now()) &
        Subscription.error.is_null() &
        Subscription.email.is_null(False)) \
        .order_by(Subscription.transaction_date.desc())

    for db_subscription in query:  # pylint: disable=too-many-nested-blocks
        process_transfer(cybersource, db_subscription)


if __name__ == '__main__':
    main()
