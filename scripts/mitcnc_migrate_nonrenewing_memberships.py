import logging
from decimal import Decimal

import environ
from google.cloud import bigquery

from cybersource_to_stripe.stripe import Stripe
from cybersource_to_stripe.utils import setup_logging

env = environ.Env(
    LOG_FILENAME=(str, 'mitcnc_migrate_nonrenewing_memberships.log'),
    STRIPE_LIVE_MODE=(bool, False),
)
environ.Env.read_env()

setup_logging(env('LOG_FILENAME'))
logger = logging.getLogger(__name__)

STRIPE_LIVE_MODE = env('STRIPE_LIVE_MODE')
PRICE_MAP = {
    '0.00': 'price_1HQOrgCAwi9Ti4sN3yrAIBCB',
    '30.00': 'price_1HQiKkCAwi9Ti4sNmXrfw8Lq',
    '50.00': 'price_1HQOrgCAwi9Ti4sN3yrAIBCB',
    '100.00': 'price_1HQOrdCAwi9Ti4sNi3suOw1n',
    '200.00': 'price_1HQOraCAwi9Ti4sNBJONrQQu',
    '500.00': 'price_1HQOrWCAwi9Ti4sNL1qY4QZ2',
    '1200.00': 'price_1HQOrjCAwi9Ti4sNmZulkTvZ',
    '3600.00': 'price_1HQOrCCAwi9Ti4sNsS5wVehZ',
} if STRIPE_LIVE_MODE else {
    '0.00': 'price_1GvfsYCAwi9Ti4sNjHt6djSk',
    '30.00': 'price_1HQiKHCAwi9Ti4sNhPBn03kC',
    '50.00': 'price_1GvfsYCAwi9Ti4sNjHt6djSk',
    '100.00': 'price_1GvfssCAwi9Ti4sNbrmnSwPF',
    '200.00': 'price_1Gvft7CAwi9Ti4sNbZnKHHCC',
    '500.00': 'price_1GvftqCAwi9Ti4sN4kAHHYKD',
    '1200.00': 'price_1GyB2GCAwi9Ti4sNokX32imb',
    '3600.00': 'price_1GyB2lCAwi9Ti4sNkNRY048w',
}


def main():
    client = bigquery.Client()
    query = """
    SELECT
      m.advance_id,
      m.first_name,
      m.last_name,
      LOWER(m.email) AS email,
      m.purchase_price,
      last_purchased_at,
      m.expires_at,
      c.id AS customer_id
    FROM
      reporting.memberships_annotated m
    LEFT JOIN
      reporting.wordpress_users u
    ON
      m.advance_id = u.advance_id
    LEFT JOIN
      reporting.stripe_customers c
    ON
      u.stripe_customer_id = c.id
    LEFT JOIN
      reporting.stripe_subscriptions s
    ON
      s.customer = c.id
    WHERE
      is_lifetime IS FALSE
      AND active IS TRUE
      AND auto_renew_enabled IS FALSE
      AND m.email IS NOT NULL
      AND m.email <> ''
      AND s.id IS NULL
    ORDER BY
      m.advance_id
    """
    query_job = client.query(query)

    stripe_client = Stripe(env.str('STRIPE_API_KEY'), livemode=STRIPE_LIVE_MODE)

    for row in query_job.result():
        stripe_price = PRICE_MAP.get(str(round(Decimal(row.purchase_price, ), 2)))
        customer = stripe_client.get_or_create_stripe_customer(
            email=row.email,
            name=f'{row.first_name} {row.last_name}'
        )

        billing_cycle_anchor = row.expires_at

        try:
            stripe_client.get_or_create_subscription(
                customer_id=customer['id'],
                stripe_price=stripe_price,
                metadata_unique_id={'note': f'Non-renewing migration for {row.email}'},
                billing_cycle_anchor=billing_cycle_anchor,
                # Setup a trial that ends on the next rollover date. This ensures
                # we don't charge again for subscriptions that are already paid.
                trial_end=billing_cycle_anchor,
                cancel_at_period_end=True,
            )
        except BaseException:
            logging.exception('Failed to create subscription. email=%s', row.email)


if __name__ == '__main__':
    main()
